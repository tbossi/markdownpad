var TEXT_COMMAND = 'text';
var DIFF_COMMAND = 'diff';
var PAD_COMMAND = 'pad';
var SID_COMMAND = 'sessionid';

var SEND_DELAY = 1500;

var md = window.markdownit().use(window.markdownitEmoji);
md.options.html = true;
md.options.linkify = true;
md.options.typographer = true;

var socket;
var sessionid;
var lastSavedText;

/*********************
 * Utility functions *
 *********************/
function executeOnceAfterDelay(fn, delay) {
 	var executed = false;
	return function (/* args */) {
		var args = arguments;
		if (!executed) {
			setTimeout(function () {
				fn.apply(null, args); // preserve arguments
			}, delay);
			executed = true;
		}
	};
};

function getEditorText() {
	var source = $(".editor").html();
	var div_open_regex = /<div(\s+[^>]*)*>/gi;
	var div_close_regex = /<\/div>/gi;
	var p_open_regex = /<p(\s+[^>]*)*>/gi;
	var p_close_regex = /<\/p>/gi;
	var br_regex = /<br\s*[\/]?>/gi;
	var space_regex = /&nbsp;/g;

	var lt_regex = /&lt;/g;
	var gt_regex = /&gt;/g;
	var quote_regex = /&quot;/g;
	var acute_regex = /&acute;/g;
	var amp_regex = /&amp;/g;

	source = source.replace(div_open_regex, "");
	source = source.replace(div_close_regex, "\n");
	source = source.replace(p_open_regex, "");
	source = source.replace(p_close_regex, "\n");
	source = source.replace(br_regex, "\n");
	source = source.replace(space_regex, " ");
	source = source.replace(lt_regex, "<");
	source = source.replace(gt_regex, ">");
	source = source.replace(quote_regex, '"');
	source = source.replace(acute_regex, '´');
	source = source.replace(amp_regex, "&");

	return source;
}

function setEditorText(text) {
	var lines = text.split('\n');

	$(".editor").html("");
	
	$.each(lines, function(n, elem) {
		var lt = /</gi;
		var gt = />/gi;
		var space = /\s/gi;
		var str = elem.replace(lt,"&lt;");
		str = str.replace(gt,"&gt;");
		str = str.replace(space,"&nbsp;");
		$(".editor").append( str + "<br/>" );
		resizeEditorAndOutput();
		setRenderedText();
	});
}

function setRenderedText() {
    var originalText = getEditorText();

    if (typeof originalText === "undefined" || originalText === "" || originalText === "\n") {
    	var message =
    		'<div class="intro">'+
    			'It seems this pad is empty.<br>'+
    			'<i class="fa fa-arrow-left" aria-hidden="true"></i><br>'+
    			'Just write something on the left to start!'+
    		'</div>';
    	$(".output").html(message);
    } else {
		var renderedText = md.render(originalText);
		$(".output").html(renderedText);
    }
}

function manageNewLineInsertion(event) {
	var lineWrappers = $(".editor > div");
	lineWrappers.before("<br/>");
	lineWrappers.children().unwrap();

	lineWrappers = $(".editor > p");
	lineWrappers.before("<br/>");
	lineWrappers.children().unwrap();
}

function resizeEditorAndOutput() {
	var offset = this.offsetHeight - this.clientHeight; 
    $(".editor").css('height', 'auto').css('height', this.scrollHeight + offset);
    $(".output").css('height', 'auto');
}

function setCaretPosition(input, pos) {
	// this function can handle caret positioning in element with child nodes like <div>something<br>something else</div>
	function setSmartRange(range, element, pos) {
		var children = element.childNodes;
		var c = 0;
		for (var i = 0; i < children.length; i++) {
			var oldC = c;
			c += children[i].textContent.length;
			if (c >= pos) {
				range.setStart(children[i], pos - oldC);
				break;
	        }
	    }
	}

	var range = document.createRange();
	var sel = window.getSelection();
	setSmartRange(range, input, pos);
	range.collapse(true);
	sel.removeAllRanges();
	sel.addRange(range);
	input.focus();
}

/*********************
 * Socket management *
 *********************/
function connectToSocket() {
	if (typeof padId === "undefined" || padId === "") {
		console.log("Unable to connect: pad undefined!");
		return;
	}

	socket = io.connect();
	socket.on('connect', function() {
   		socket.emit(PAD_COMMAND, padId);

   		socket.on(SID_COMMAND, function(sid) {
   			sessionid = sid;
   		})

   		socket.on(TEXT_COMMAND, onTextReceived);
   		socket.on(DIFF_COMMAND, onDiffReceived);
	});

	function onTextReceived(text) {
		console.log('Received \n' + text);
	
		if (typeof text === "undefined" || text == null)
			return;
	
		lastSavedText = text;
	
		setEditorText(text);
	}
	
	function onDiffReceived(diff) {
		//console.log('Received \n');
		//console.log(diff);
	
		if (typeof diff === "undefined" || diff == null)
			return;
	
		var oldCaretPos = $(".editor").caret('pos');
		var oldCounter = 0;
		var newCounter = 0;
		var newCaretPos = 0;
		var caretYetToSet = true;
	
		var text = '';
		
		diff.forEach(function(part) {
			if (part.removed) {
				oldCounter += part.value.length;
				if (caretYetToSet) {
					if (oldCounter >= oldCaretPos) {
						newCaretPos = newCounter;
						caretYetToSet = false;
					}
				}
			} else if (part.added) {
				text += part.value;
				newCounter += part.value.length;
			} else {
				text += part.value;
				var previousNewCounter = newCounter;
				var previousOldCounter = oldCounter;
				oldCounter += part.value.length;
				newCounter += part.value.length;
				if (caretYetToSet) {				
					if (oldCounter >= oldCaretPos) {
						newCaretPos = previousNewCounter + (oldCaretPos - previousOldCounter);
						caretYetToSet = false;
					}
				}
			}
		});
		if (caretYetToSet) {
			newCaretPos = newCounter;
		}
		if (newCaretPos > text.length) {
			newCaretPos = text.length;
		}
	
		lastSavedText = text;
		setEditorText(text);
	
		setCaretPosition($(".editor")[0], newCaretPos);
	}
}

function sendToServer(event) {
	function typedSomethingNew(event) {
		var key = event.which || event.keyCode;
	
		if (typeof key === "undefined")
			return true;
	
		var notNewText = event.crtlKey || event.altKey || event.metaKey;
		notNewText = notNewText || key == 37 || key == 38 || key == 39 || key == 40; // arrows
		notNewText = notNewText || key == 27 || key == 19 || key == 20; //esc, pause/break, caps lock
		notNewText = notNewText || key == 33 || key == 34 || key == 35 || key == 36; //page up down, end, home
		notNewText = notNewText || key == 144 || key == 145; 
		notNewText = notNewText || (key >= 112 && key <= 123); //function keys
	
		return !notNewText;
	}

	function send() {
		var curr = getEditorText();
		//console.log(curr);
		var diff = JsDiff.diffChars(lastSavedText, curr);

		//console.log("Sending to server: ");
		//console.log(diff);

		socket.emit(DIFF_COMMAND, {
			sessionId: sessionid,
			diff: diff,
		});

		lastSavedText = curr;
	}

	if (typeof socket === "undefined") {
		console.log("Unable to connect: socket undefined!");
		return;
	}
	if (typeof event !== "undefined" && !typedSomethingNew(event))
		return;
	
	var delayedSend = executeOnceAfterDelay(send, SEND_DELAY);
	delayedSend();
}

/*************************
 * Event listeners setup *
 *************************/
function initializeListeners() {
	$(".editor").css('min-height', $(".editor").first().scrollHeight);

	$(".editor").on("change input", manageNewLineInsertion); // browsers must insert <br> instead of <div> or <p> on Enter

	$(".editor").on("change input", sendToServer);
	$(".editor").on("change input", resizeEditorAndOutput);
	$(".editor").on("change input", setRenderedText);

	var shareUrl = new Clipboard('#share', {
    	text: function(trigger) {
			return window.location.href;
		}
	});

	setRenderedText();
	resizeEditorAndOutput();
}

$(document).ready(function() {
	connectToSocket();
	initializeListeners();
});