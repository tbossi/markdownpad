'use strict';

class BaseController {
	sendResult(req, res) {
	}

	send404page(req, res) {
		res.status(404).render('404', {});
	}
}

module.exports = BaseController;