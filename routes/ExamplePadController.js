'use strict';
const BaseController = require('./BaseController');

class ExamplePageController extends BaseController {
	sendResult(req, res) {
		res.setHeader('Content-Type', 'text/html'); 
        res.render('demo', {});
	}
}

module.exports = ExamplePageController;