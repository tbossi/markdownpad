'use strict';
const BaseController = require('./BaseController');

class HomePageController extends BaseController {
	
	sendResult(req, res) {
		res.setHeader('Content-Type', 'text/html'); 
        res.render('home', {});
	}
}

module.exports = HomePageController;