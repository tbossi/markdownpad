'use strict';
const BaseController = require('./BaseController');

class PadController extends BaseController {
	constructor(padIdIdentifier, firebaseAdmin, webSocketManager) {
		super();
		this.padIdIdentifier = padIdIdentifier;
		this.firebase = firebaseAdmin;
		this.padsRef = this.firebase.database().ref('pad');

		this.webSocketManager = webSocketManager;
	}

	getPadId(req) {
		return req.params[this.padIdIdentifier];
	}

	sendResult(req, res) {
		var padId = this.getPadId(req);

		var padRef = this.padsRef.child(padId);
		var thisPadController = this;

		padRef.once('value').then(function(snapshot) {
			if (snapshot.exists())
				thisPadController.sendFoundResult(req, res, snapshot);
			else
				thisPadController.sendNotFoundResult(req, res);
		});   
	}

	sendNotFoundResult(req, res) {
		super.send404page(req, res);
	}

	sendFoundResult(req, res, snapshot) {
		var creationDate = snapshot.child('creationDate').val();
		var content = snapshot.child('content').val();

		this.webSocketManager.registerForPad(this.getPadId(req), this.firebase);

		res.render('pad', {
			padId: this.getPadId(req),
			creationDate: creationDate,
		});
	}
}

module.exports = PadController;