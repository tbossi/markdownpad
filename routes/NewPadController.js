'use strict';
const moment = require('moment');
const BaseController = require('./BaseController');

class NewPadController extends BaseController {
    constructor(firebaseAdmin) {
        super();
        this.firebase = firebaseAdmin;
        this.padsRef = this.firebase.database().ref('pad');
    }

    createPadInFirebase() {
        /*La push di firebase si preoccupa di creare un id univoco!*/
        return this.padsRef.push({
            creationDate: moment.utc( new Date() ).format(),
            content: "",
            connections: "",
        }).key;
    }

    sendResult(req, res) {
    	var padId = this.createPadInFirebase();
        res.redirect('/pad/' + padId);
    }
}

module.exports = NewPadController;