'use strict';
const jsdiff = require('diff');

const TEXT_COMMAND = 'text';
const DIFF_COMMAND = 'diff';
const PAD_COMMAND = 'pad';
const SID_COMMAND = 'sessionid';

class WebSocket {
	constructor(ioSocket) {
		this.ioSocket = ioSocket;

    	this.padSockets = {};
    	this.clients = {};
	}

	registerForPad(padId, firebase) {
		var padsockets = this.padSockets;
		var socket = this.ioSocket;
		
		if (typeof padsockets[padId] === "undefined" || padsockets[padId] == null) {
			padsockets[padId] = new PadSocketManager(socket, padId, firebase);
		}
	}

	startListening() {
		var padSockets = this.padSockets;
		var clientList = this.clients;
		var iosocket = this.ioSocket;
		
		var padAccess = this.onPadAccess;
		var textCommand = this.onTextCommand;
		var diffCommand = this.onDiffCommand;
		var fun = function() {
			iosocket.on('connection', function(socket) {
    	    	socket.on(PAD_COMMAND, padAccess(socket, clientList, padSockets));
        		socket.on(TEXT_COMMAND, textCommand(socket, clientList, padSockets));
        		socket.on(DIFF_COMMAND, diffCommand(socket, clientList, padSockets));
    		});
		};
		process.nextTick(fun);
	}
	
	onPadAccess(socket, clients, padSockets) {
		return function(pad) {
			var fun = function() {
				if (!(typeof padSockets[pad] === "undefined")) {
					socket.join(pad);

           			var clientId = padSockets[pad].onAccess(socket);
           			clients[clientId] = pad;
           	
           			socket.emit(SID_COMMAND, clientId);
           			var txt = padSockets[pad].getText();
           			socket.emit(TEXT_COMMAND, txt);
				} else {
           			socket.emit('failure', 'Pad ' + pad + ' does not exists!');
				}
			};
			process.nextTick(fun);
		}
	}

	onTextCommand(socket, clients, padSockets) {
		return function(data) {
			var fun = function() {
				var sessionId = data['sessionId'];
				var clientRoom = clients[sessionId];

				if (!(typeof padSockets[clientRoom] === "undefined")) {
					var text = data['text'];
					padSockets[clientRoom].onText(text, socket);	
				} else {
					console.log('Error: pad unavailable! ' + data);			
				}
			};
			process.nextTick(fun);
		}
	}

	onDiffCommand(socket, clients, padSockets) {
		return function(data) {
			var fun = function() {
				var sessionId = data['sessionId'];
				var clientRoom = clients[sessionId];

				if (!(typeof padSockets[clientRoom] === "undefined")) {
					var diff = data['diff'];
					padSockets[clientRoom].onDiff(diff, socket);	
				} else {
					console.log('Error: pad unavailable! ' + data);			
				}
			};
			process.nextTick(fun);
		}
	}
}

class PadSocketManager {
	constructor(ioSocket, padId, firebase) {
		this.padId = padId;

		this.firebase = firebase;
		this.padRef = this.firebase.database().ref('pad').child(this.padId);
		this.content = '';

		this.io = ioSocket;

		this.retriveUpdates();
	}

	retriveUpdates() {
		var psm = this;
		this.padRef.child('content').on("value", function(snapshot) {
			var text = snapshot.val();
			psm.setText(text);
		});
	}

	storeText(text) {
		this.padRef.set({content: text});
	}

	getText() {
		return this.content;
	}

	setText(text) {
		this.content = text;
	}

	onAccess(clientSocket) {
		var sessionid = this.padRef.child('connections').push({
			connected : true,
		}).key;
		return sessionid;
	}

	onDisconnect(clientSocket) {
		this.clientList.remove(clientSocket);
	}

	onText(text, authorSocket) {
		if (typeof text === "undefined" || text == null)
			return;
		
		// sending to all clients in this pad except sender
		authorSocket.broadcast.to(this.padId).emit(TEXT_COMMAND, text);
		
		this.storeText(text);
	}

	onDiff(diff, authorSocket) {
		if (typeof diff === "undefined" || diff == null)
			return;

		// sending to all clients in this pad except sender
		authorSocket.broadcast.to(this.padId).emit(DIFF_COMMAND, diff);

		var text = '';
		diff.forEach(function(part) {
			if (!part.removed) {
				text += part.value;
			}
		});
		this.storeText(text);	
	}
}

module.exports = WebSocket;