#!/bin/env node --harmony
"use strict";
const http        = require('http');
const compression = require('compression');
const express     = require('express');
const socketio    = require('socket.io');
const consolidate = require('consolidate')
const fs          = require('fs');
const admin       = require("firebase-admin");

const serviceAccount = require("./markdownpad-firebase-key.json");
const databaseUrl = "https://markdownpad-4d45d.firebaseio.com";

var MarkdownPadServer = function() { 
    //  Scope. 
    var self = this;
    self.name = 'MarkdownPad';
    /** 
     *  Set up server IP address and port using env variables if available. 
     */ 
    self.setupVariables = function() { 
        self.ipaddress = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'; 
        self.port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080; 

        self.routes = { };
    }; 

    /** 
     *  Populate the cache. 
     */ 
    self.populateCache = function() { 
        if (typeof self.zcache === "undefined") { 
            self.zcache = {
                'markdown_editor.js': '',
                'main.css' : '',
                'caret.js' : '',
                'diff.js' : '',
            }; 
        } 

        //  Local cache for static content. 
        self.zcache['markdown_editor.js'] = fs.readFileSync('./public/script/markdown_editor.js'); 
        self.zcache['main.css'] = fs.readFileSync('./public/style/main.css');
        self.zcache['caret.js'] = fs.readFileSync('./node_modules/jquery.caret/dist/jquery.caret.min.js');
        self.zcache['diff.js'] = fs.readFileSync('./node_modules/diff/dist/diff.min.js');
    }; 

    /** 
     *  Retrieve entry (content) from cache. 
     *  @param {string} key  Key identifying content to retrieve from cache. 
     */ 
    self.cache_get = function(key) { return self.zcache[key]; }; 

    /** 
     *  terminator === the termination handler 
     *  Terminate server on receipt of the specified signal. 
     *  @param {string} sig  Signal to terminate on. 
     */ 
    self.terminator = function(sig) { 
        if (typeof sig === "string") { 
            console.log('%s: Received %s - terminating '+self.name +' app ...', 
                Date(Date.now()), sig); 
            process.exit(1); 
        } 
        console.log('%s: Node server stopped.', Date(Date.now()) ); 
    }; 

    /** 
     *  Setup termination handlers (for exit and a list of signals). 
     */ 
    self.setupTerminationHandlers = function(){ 
        //  Process on exit and signals. 
        process.on('exit', function() { self.terminator(); }); 

        // Removed 'SIGPIPE' from the list - bugz 852598. 
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 
        'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM' 
        ].forEach(function(element, index, array) { 
            process.on(element, function() { self.terminator(element); }); 
        }); 
    }; 

    /*  ================================================================  */ 
    /*  App server functions (main app logic here).                       */ 
    /*  ================================================================  */ 
    self.addRoute = function(path, method, callback) {
    	self.routes[path] = {
            method: method,
            action: callback
        };

    	if (!(typeof self.app === "undefined" || self.app == null)) {
            if (method === 'GET')
        		self.app.get(path, self.routes[path].action);
            else if (method === 'POST')
                self.app.post(path, self.routes[path].action);
            else
                throw 'Method ' + method + 'not allowed. Use GET or POST.';
    	} else
            throw 'Can\'t add Route: Express app is not initialized!';
    };

    self.initializeServer = function() { 
        self.app = express(); 

        self.httpServer = http.createServer(self.app);
        self.ioSocket = socketio(self.httpServer); 

        self.app.use(compression());
        self.app.engine('html', consolidate.mustache);
        self.app.set('view engine', 'html');
        self.app.set('views', __dirname + '/views');

        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: databaseUrl,
        });
    }; 

    self.initialize = function() { 
        self.setupVariables(); 
        self.populateCache(); 
        self.setupTerminationHandlers(); 

        // Create the express server and routes. 
        self.initializeServer();

        self.addRoute('/style/main.css', 'GET', function(req, res) {
            res.setHeader('Content-Type', 'text/css');  
            res.send(self.cache_get('main.css'));
        });
        self.addRoute('/script/markdown_editor.js', 'GET', function(req, res) {
            res.setHeader('Content-Type', 'text/javascript');  
            res.send(self.cache_get('markdown_editor.js'));
        });
        self.addRoute('/script/jquery.caret.min.js', 'GET', function(req, res) {
            res.setHeader('Content-Type', 'text/javascript');  
            res.send(self.cache_get('caret.js'));
        });
        self.addRoute('/script/diff.min.js', 'GET', function(req, res) {
            res.setHeader('Content-Type', 'text/javascript');  
            res.send(self.cache_get('diff.js'));
        });
    }; 

    self.start = function() { 
        console.log(self.name + ' Server starting...');

        self.httpServer.listen(self.port, self.ipaddress, function() { 
            console.log('%s: Node server started on %s:%d ...', 
                Date(Date.now() ), self.ipaddress, self.port);
        });
    }; 
};

/** 
 * Main code. 
 */
function initServer() {
    var server = new MarkdownPadServer();

    console.log('Initializing ' + server.name + ' server...');
    server.initialize();

    // websocket
    console.log('Setting up WebSocket manager...');
    
    const WebSocket = require('./websocket/websocket');
    var wsmanager = new WebSocket(server.ioSocket);
    wsmanager.startListening();

    // routes
    console.log('Adding routes...');

    const HomePageController = require('./routes/HomePageController');
    var homePageController = new HomePageController();
    server.addRoute('/', 'GET', function(req, res) {
        homePageController.sendResult(req, res);    
    });
    const NewPadController = require('./routes/NewPadController');
    var newPadController = new NewPadController(admin);
    server.addRoute('/pad/newpad', 'GET', function(req, res) {
        newPadController.sendResult(req, res);    
    });
    const ExamplePadController = require('./routes/ExamplePadController');
    var examplePadController = new ExamplePadController();
    server.addRoute('/pad/demo', 'GET', function(req, res) {
        examplePadController.sendResult(req, res);    
    });
    const PadController = require('./routes/PadController');
    var padController = new PadController('padId', admin, wsmanager);
    server.addRoute('/pad/:padId([A-Za-z0-9_-]{20})', 'GET', function(req, res) {
        padController.sendResult(req, res);    
    });

    // All other routes give a 404 error page
    const BaseController = require('./routes/BaseController');
    var baseController = new BaseController();
    server.addRoute('/*', 'GET', function(req, res) {
        baseController.send404page(req, res);    
    });

    server.start();
}

initServer();